$(function() {

  // Setea valores al modal de #competencia
  $('.page-busqueda-persona').on('click', '.ver-competencia', function() {
    $('#msg-reportar-competencia').html('');
    var dataJson = JSON.parse($(this).attr('data-competencia'));

    $('#modal-competencia .modal-title').text(dataJson.name_competencia);
    $('#modal-competencia #expedition_date').text(dataJson.expedition_date);
    $('#modal-competencia #expiration_date').text(dataJson.expiration_date);
    $('#modal-competencia #validity_time').text(dataJson.validity_time);
    $('#modal-competencia #comment_report').text(dataJson.comment_report);

    if (dataJson.attached_certificate == null) {
      $('#modal-competencia #attached_certificate').click(function(event) {
          event.preventDefault();
      });

      $('#modal-competencia #attached_certificate img').hide();
      $('#modal-competencia #attached_certificate p').show();
    }else {
      $('#modal-competencia #attached_certificate img').show();
      $('#modal-competencia #attached_certificate p').hide();
      var url = '/application/public/src/certificados/' + dataJson.attached_certificate
      $('#modal-competencia #attached_certificate').attr('href', url);
    }

    $('#modal-competencia #submit-reportar').click(function(event) {
      $.ajax({
        type: 'POST',
        url: '/reportar-competencia',
        data: {
          id: dataJson.id,
          comment_report: $('#modal-competencia #comment_report').val(),
        },
        beforeSend: function(){
          //$(this).siblings('.loading').show();      
        },
        success: function(data){
          console.log(data);

          try {
            var dataJson = JSON.parse(data);
            $('#msg-reportar-competencia').html(dataJson.msg);
            
            setTimeout(function(){ 
              location.reload();
            }, 1500);

          } catch (e) {
            $('#msg-reportar-competencia').html('Error al reportar');
          }
        },   
        complete: function(){
          //$(this).siblings('.loading').hide();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          //$('#msg-delete').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
        }
      });
    });
  });
});