var dataOBJ = {},
    tbl = null;

$(function() {
  if ($("#tbl-" + str_MODULE).length > 0) {

    // --------------------------------------------------------------------------
    // --------------------------------------------------------------------------
    // ---------------------------- TABLA ------------------------------
    // --------------------------------------------------------------------------
    tbl = $('#tbl-' + str_MODULE).DataTable({
      'paging': true,
      'info': true,
      'filter': true,
      'stateSave': true,
      'processing': true, 
      'serverSide': true,
      'lengthMenu' : [[10, 30, 50, -1], [10, 30, 50, 'Todo']], // edita el menu de cantidad de registros que se muestran
      dom: 'Bfrtlip',
      buttons: [
          {
              extend: 'excelHtml5',
              text: str_exportExcel,
              title: str_exportTitle,
              exportOptions: {
                  columns: [0, 1, 2, 3]
              }
          },
          {
              extend: 'pdfHtml5',
              text: str_exportPdf,
              title: str_exportTitle,
              exportOptions: {
                  columns: [0, 1, 2, 3]
              }
          }
      ],
      'ajax': {
        'url': '/' + str_URL_1 + '/ssp-index',
        'type': 'POST',
        'data': function (d) {
          return $.extend({}, d, {
            'dateStart': $('#dateStart').val(),
            'dateEnd': $('#dateEnd').val(),
          } );
        },
      },
      'columns': [
        {data: 'id', 'sClass': 'dt-body-center'},
        {data: 'name', 'sClass': 'dt-body-center'},
        {data: 'reference', 'sClass': 'dt-body-center'},
        {data: 'price', 'sClass': 'dt-body-center'},
        {data: 'cost', 'sClass': 'dt-body-center'},
        {data: 'current_units', 'sClass': 'dt-body-center'},
        {data: 'state', 'sClass': 'dt-body-center'},
        {data: 'name_tipo_producto', 'sClass': 'dt-body-center'},
      ],
       "oLanguage": {
          "sProcessing":     oLanguage.sProcessing,
          "sLengthMenu":     oLanguage.sLengthMenu,
          "sZeroRecords":    oLanguage.sZeroRecords,
          "sEmptyTable":     oLanguage.sEmptyTable,
          "sInfo":           oLanguage.sInfo,
          "sInfoEmpty":      oLanguage.sInfoEmpty,
          "sInfoFiltered":   oLanguage.sInfoFiltered,
          "sInfoPostFix":    "",
          "sSearch":         oLanguage.sSearch,
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": oLanguage.sLoadingRecords,
          "oPaginate": {
              "sFirst":    oLanguage.oPaginate.sFirst,
              "sLast":     oLanguage.oPaginate.sLast,
              "sNext":     oLanguage.oPaginate.sNext,
              "sPrevious": oLanguage.oPaginate.sPrevious
          },
          "oAria": {
              "sSortAscending": oLanguage.oAria.sSortAscending,
              "sSortDescending": oLanguage.oAria.sSortDescending
          }
      },
      "language": {
        "url": "dataTables.german.lang"
      },
      'order': [[0, 'desc']],
    });

    $("#tbl-" + str_MODULE + "_filter").append('<label>' +
          '<span style="margin-left:5px;">' + str_start + '</span>' +
          '<input type="text" id="dateStart" name="dateStart" readonly="true">' +
        '</label>' +
        '<label>' +
          '<span style="margin-left:5px;">' + str_end + '</span>' +
          '<input type="text" id="dateEnd" name="dateEnd" readonly="true">' +
        '</label>');

  
    $("#dateStart, #dateEnd").datepicker({
      changeMonth: true,
      changeYear: true,
      regional: 'es',
      dateFormat: 'yy-mm-dd',
      showButtonPanel: true,
      closeText: str_closeText,
      onClose: function () {
        var event = arguments.callee.caller.caller.arguments[0];

        if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
          $("#dateStart").val('');
          $("#dateEnd").val('');
          tbl.draw();
        }
      }
    });

    $('#dateStart, #dateEnd').change(function () {
      if ($("#dateStart").val() != "" && $("#dateEnd").val() != "") {
        tbl.draw();
      }
    });
  }// endif table-users
});


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// --------- OBTENER DIFERENCIA DE DIAS ENTRE 2 FECHAS ----------------------
// --------------------------------------------------------------------------
getDifferenceDays = function(f1,f2) {
  var aFecha1 = f1.split('-'); 
  var aFecha2 = f2.split('-'); 
  var fFecha1 = Date.UTC(aFecha1[0],aFecha1[1]-1,aFecha1[2]); 
  var fFecha2 = Date.UTC(aFecha2[0],aFecha2[1]-1,aFecha2[2]); 
  var dif = fFecha2 - fFecha1;
  var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
  
  return dias;
}


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// -------------------------- OBTENER FECHA DE HOY --------------------------
// --------------------------------------------------------------------------
function getToday() {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();

  if(dd<10) {
    dd = '0'+dd
  } 

  if(mm<10) {
    mm = '0'+mm
  } 

  return yyyy + '-' + mm + '-' + dd;
}


