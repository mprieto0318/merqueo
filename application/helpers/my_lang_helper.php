<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Change Language -> <?= anchor(base_url() . 'user/Clanguage/index/es?redirect=' . uri_string(), 'Español'); ?>

   echo _("bienvenido a mi app");
   echo "<br>";
   echo sprintf(_("saludo usuario %s"),"Juan");
   echo "<br>";
   echo sprintf(_("hola %s hoy es %s"),"Juan", date('Y-m-d'));
   echo "<br>Pluralización: ";
   //primer parámetro indice[0],segundo indice[1], tercero, más de uno plural
   //el último reemplaza por el comodín sprintf
   echo sprintf(ngettext("producto tienda", "productos tiendas", 2), 2);
 */

if (!function_exists('change_language')) {

  function change_language($lang = 'en_US') {
    $CI =& get_instance();
   
    $data = array(
        'lang' => $lang,
    );

    $CI->session->set_userdata($data);
  }
}

if (!function_exists('load_language')) {
  function load_language($language = 'es_CO') {
    $CI =& get_instance();
    
    $directorioArchivos = APPPATH.'language/locale';
    $codification = '';

    if ($language == 'es_CO') {
      $codification = '.UTF-8';
    }

    /*
    putenv('LANG='.$language.'.UTF-8');
    setlocale(LC_ALL, $language.$codification);
    bindtextdomain('lang', $directorioArchivos);
    textdomain('lang');//nombre de los archivos
    */

    //Depending on your OS, putenv/setlocale/both will set your language.
    putenv('LC_ALL=' . $language);
    setlocale(LC_ALL, $language);

    bindtextdomain( "lang", APPPATH."language/locale" ); //set the locale folder for a textdomain
    bind_textdomain_codeset( "lang", "UTF-8" ); //set the codeset for a textdomain
    textdomain( "lang" ); //choose a textdomain
  }
}

if (!function_exists('selected_lang')) {
  function selected_lang($lang = 'en_US') {
    $CI =& get_instance();
    if ($CI->session->userdata('lang')) {
      $lang = $CI->session->userdata('lang');
    }

    return $lang;
  }
}