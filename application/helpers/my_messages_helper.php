<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * MODO DE USO
    // - Guardar Mensaje set_flashdata 
    // - Llamar funcion del Helper "my_messages"
    // - Type Messages: success, info, warning, danger
    $type = 'success';
    $msg = $this->upload->display_errors();
    save_messages($type, $msg);
 */



// - !Cargar este helper en el autoload
if (!function_exists('render_messages')) {

  /*
   * Type Messages: success, info, warning, danger
   */
  function render_messages() {
    $CI =& get_instance();
    
    $html = '<div class="wrap-messages">';

    foreach ($CI->session->flashdata() as $key => $type) {
      foreach ($type as $msg) {
        $html .= build_message($key, $msg);
      }
    }

    $html .= '</div>';

    return $html;
  }
}

if (!function_exists('build_message')) {
  function build_message($type, $msg) {

  	return '<div class="alert alert-' . $type . ' alert-dismissable fade in">
        	    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        	    ' . $msg . '
        	 </div>';

  }
}

if (!function_exists('save_messages')) {
  function save_messages($type, $msg) {
    $CI =& get_instance();
    $flash_msg[$type][] = $msg;
    $CI->session->set_flashdata($flash_msg);
  }
}

