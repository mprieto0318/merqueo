<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * ----- MODO DE USO -------

    $inputs = $this->input->post();

	$values = array(
	  'nombre_u' => $inputs['name'], 
	  'img_u' => $result_img,
	  ...
	);

    $this->load->helper('my_clean_data');
    $table = 'usuarios';
    $result = $this->Mregistro->create($table, $this->clean_data($values));

    if ($result > 0) {
	  // SUCCESS
    }else {
      // ERROR
    }
 */


if (!function_exists('clean_data')) {
  function clean_data($values) {
    $CI =& get_instance();
    $clean = array();
    foreach ($values as $key => $value) {
      $clean[$key] = $CI->security->xss_clean(strip_tags($value));
    }
    return $clean;
  }
}