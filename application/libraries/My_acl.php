<?php
/**
 *
 * Dynmic_menu.php
 * Created By Mprieto -  www.mprieto.co
 */
class My_acl {

  private $ci;
  private $roles;

  function __construct() {
    $this->ci =& get_instance();
    $this->roles = array(
      'VISITOR'   => 1,
      'OPERATOR'  => 2,
      'ADMIN'     => 3,
      'FULLADMIN' => 4,
    );
  }

  function access_values($module = FALSE, $class = FALSE, $method = FALSE) {
    $acl = array(
      /*'lang' => array(
        'controllers' => array(
          'Lang' => array(
            'index' => 'VISITOR',
          ),
        ),
      ),*/
      'inventario' => array(
        'controllers' => array(
          'Inventario' => array(
            'index' => 'VISITOR',
            'ssp_index' => 'VISITOR',
            'csv' => 'VISITOR',
          ),
        ),
      ),
      'home' => array(
        'controllers' => array(
          'Home' => array(
            'index' => 'OPERATOR',
            'search_byCC' => 'OPERATOR',
            'reportar_competencia' => 'ADMIN',
          ),
        ),
      ),
      'dashboard' => array(
        'controllers' => array(
          'Dashboard' => array(
            'index' => 'OPERATOR',
          ),
        ),
      ),
      'random' => array(
        'controllers' => array(
          'Random' => array(
            'index' => 'VISITOR',
            'generate' => 'VISITOR',
          ),
        ),
      ),
      'repeat' => array(
        'controllers' => array(
          'Repeat' => array(
            'index' => 'VISITOR',
            'generate' => 'VISITOR',
          ),
        ),
      ),
      'athlete' => array(
        'controllers' => array(
          'Athlete' => array(
            'index' => 'VISITOR',
          ),
        ),
      ),
      'user' => array(
        'controllers' => array(
          'User' => array(
            'index' => 'VISITOR', // view login
            'recover_pass' => 'VISITOR',
            'login' => 'VISITOR',
            'logout' => 'OPERATOR',
          ),
          'Admin' => array(
            'index' => 'OPERATOR',
            'ssp_list_users' => 'OPERATOR',
            'create_user_ajax' => 'ADMIN',
            'update_user_ajax' => 'ADMIN',
            'update_security_user_ajax' => 'ADMIN',
            'delete_user_ajax' => 'ADMIN',
            'change_pass_token' => 'VISITOR',
          ),
        ),
      ),
      'support' => array(
        'controllers' => array(
          'Support' => array(
            'form_mail' => 'VISITOR',
            'select_mail' => 'VISITOR',
          ),
        ),
      ),
      'empresascol' => array(
        'controllers' => array(
          'Empresascol' => array(
            'index' => 'OPERATOR',
            'ssp_list' => 'OPERATOR',
            'create_ajax' => 'ADMIN',
            'update_ajax' => 'ADMIN',
            'delete_ajax' => 'ADMIN',
          ),
        ),
      ),
      'cargos' => array(
        'controllers' => array(
          'Cargos' => array(
            'index' => 'OPERATOR',
            'ssp_list' => 'OPERATOR',
            'create_ajax' => 'ADMIN',
            'update_ajax' => 'ADMIN',
            'delete_ajax' => 'ADMIN',
          ),
        ),
      ),
      'nivelacademico' => array(
        'controllers' => array(
          'Nivelacademico' => array(
            'index' => 'OPERATOR',
            'ssp_list' => 'OPERATOR',
            'create_ajax' => 'ADMIN',
            'update_ajax' => 'ADMIN',
            'delete_ajax' => 'ADMIN',
          ),
        ),
      ),
      'competencias' => array(
        'controllers' => array(
          'Competencias' => array(
            'index' => 'OPERATOR',
            'ssp_list' => 'OPERATOR',
            'create_ajax' => 'ADMIN',
            'update_ajax' => 'ADMIN',
            'delete_ajax' => 'ADMIN',
          ),
        ),
      ),
      'personal' => array(
        'controllers' => array(
          'Personal' => array(
            'index' => 'OPERATOR',
            'ssp_list' => 'OPERATOR',
            'create_ajax' => 'ADMIN',
            'create_competencia_ajax' => 'ADMIN',
            'get_edit_competencia_ajax' => 'ADMIN',
            'edit_competencia_ajax' => 'ADMIN',
            'delete_competencia_ajax' => 'ADMIN',
            'list_competencias' => 'ADMIN',
            'update_ajax' => 'ADMIN',
            'update_competencias_ajax' => 'ADMIN',
            'delete_ajax' => 'ADMIN',
            'download_file' => 'ADMIN',
          ),
        ),
      ),
    );
      

    if ($module && $class && $method) {

      if (isset($acl[$module]['controllers'][$class][$method])) {
        return $acl[$module]['controllers'][$class][$method];
      }
        
      $type = 'danger';
      $info = '<ul><li>Module: ' . $module . '</li><li>Class: ' . $class . '</li><li>Method: ' . $method . '</li></ul>';
      $msg = _('The module has not been defined or verify that the url matches the module<br>') . $info;
      save_messages($type, $msg);
      redirect('/');
    }
   
    return $acl;
  }


  /*
   * @params
   * --- $redirect = si se desea redirigir o retornar TRUE o FALSE
   * --- $module_get = nombre del modulo
   * --- $class_get = nombre de la clase del modulo
   * --- $method_get = nombre del metodo de la clase
   *
   * //// USO /////
   *
   * - Nota: se debe llamar de la misma forma que se declara el routes.php: 
   * -- $route['user/list-of-users'] = 'user/Admin/index';
   *
   * - Para validar si mostrar contenido llamar el metodo asi:
   *
   * <?php if($this->my_acl->access_control(FALSE, 'user', 'Admin', 'index')) : ?>
   *   <div>Mi Contenido</div>
   * <?php endif; ?>
   */
  function access_control($redirect = TRUE, $module_get = FALSE, $class_get = FALSE, $method_get = FALSE){


    $rol_user = 1;
    if ($this->ci->session->userdata('roles_id') !== NULL) {
      $rol_user = $this->ci->session->userdata('roles_id');
    }

    if ($module_get && $class_get && $method_get) {
      $module = $module_get;
      $class = $class_get;
      $method = $method_get;
    }else {
      $module = $this->ci->router->fetch_module();
      $class = $this->ci->router->fetch_class();
      $method = $this->ci->router->fetch_method();
    }

    

    /*
    echo $module;
    echo '<br>';
    echo $class;
    echo '<br>';
    echo $method;
    echo '<br>';

    echo '<hr>';
    echo $rol_user;
    echo '<hr>';
    echo $this->roles[$this->access_values($module, $class, $method)];

    exit;
  */


    $validate_access = TRUE;

    if ($this->access_values($module, $class, $method) == 'VISITOR') {
      return true;
    }
    if (validate_session() && $module == 'user' && $class == 'User' && $method == 'index') {
      $validate_access = FALSE;
    }

    if ($validate_access) {

        /*if ($module == 'competencia') {
          echo $module;
          echo '<br>';
          echo $class;
          echo '<br>';
          echo $this->ci->router->fetch_class();
          echo '<br>';
          echo $method;
          echo '<br>';

          echo '<hr>';
          echo $rol_user;
          echo '<hr>';
        }*/

      
        if ($rol_user < $this->roles[$this->access_values($module, $class, $method)]) {
          if ($redirect) {
            $type = 'danger';
            $msg = _('you do not have permission to access the module');
            save_messages($type, $msg);
            
            redirect('/');
          }

          return FALSE;
        }
      
      
    }
    
    return TRUE;
  }
}