<?php
/**
 *
 * Dynmic_menu.php
 * Created By Mprieto -  www.mprieto.co
 */
class My_menu {

  private $ci;
  private $roles;

  function __construct() {
    $this->ci =& get_instance();
    $this->roles = array(
      'VISITOR'   => 1,
      'OPERATOR'  => 2,
      'ADMIN'     => 3,
      'FULLADMIN' => 4,
    );
  }

  function options_menu() {
    
    //$uri = ($_SERVER['REQUEST_URI'] != '/') ? $_SERVER['REQUEST_URI'] : '';
    $base_url = (base_url() !== null) ? base_url() : BASE_URL;
    //$base_url = BASE_URL;

    $redirect = $this->ci->my_utilities->get_full_url();

    $langEn = '';
    $langCo = '';
    $langFr = '';

    /*switch (selected_lang()) {
      case 'en_US':
        $langEn = '<i class="fa fa-check" aria-hidden="true"></i>';
        break;
      case 'es_CO':
        $langCo = '<i class="fa fa-check" aria-hidden="true"></i>';
        break;
      case 'fr_FR':
        $langFr = '<i class="fa fa-check" aria-hidden="true"></i>';
        break;
      
      default:
        $langEn = '<i class="fa fa-check" aria-hidden="true"></i>';
        break;
    }*/


    $menu = array(
      array(
        'title' => _('Search'),
        'route' => 'busqueda',
        'module' => 'home',
        'access' => 'OPERATOR',
      ),
      array(
        'title' => _('Login'),
        'route' => '_MODAL',
        'id_modal' => 'modal-login',
        'module' => 'user',
        'access' => 'VISITOR',
        'id' => 'login'
      ),
      array(
        'title' => _('Dashboard'),
        'route' => 'dashboard',
        'module' => 'dashboard',
        'access' => 'OPERATOR',
      ),
      array(
        'title' => _('Modules'),
        'route' => '#',
        'module' => 'user',
        'access' => 'VISITOR',
        'pages' => array(
          array(
            'title' => _('Random'),
            'route' => 'random',
            'module' => 'random',
            'access' => 'VISITOR',
          ),
          array(
            'title' => _('Repeat'),
            'route' => 'repeat',
            'module' => 'repeat',
            'access' => 'VISITOR',
          ),
          array(
            'title' => _('Athlete'),
            'route' => 'athlete',
            'module' => 'athlete',
            'access' => 'VISITOR',
          ),
          array(
            'title' => _('Collaborating Companies'),
            'route' => 'empresas-colaboradoras',
            'module' => 'empresascol',
            'access' => 'OPERATOR',
          ),
          array(
            'title' => _('Appointment'),
            'route' => 'cargos',
            'module' => 'cargos',
            'access' => 'OPERATOR',
          ),
          array(
            'title' => _('Academic Level'),
            'route' => 'nivel-academico',
            'module' => 'nivelacademico',
            'access' => 'OPERATOR',
          ),
          array(
            'title' => _('Competitions'),
            'route' => 'competencias',
            'module' => 'competencias',
            'access' => 'OPERATOR',
          ),
          array(
            'title' => _('Personal'),
            'route' => 'personal',
            'module' => 'personal',
            'access' => 'OPERATOR',
          ),
        ),
      ),
      array(
        'title' => _('System'),
        'route' => '#',
        'module' => 'user',
        'access' => 'OPERATOR',
        'pages' => array(
          array(
            'title' => _('Users'),
            'route' => 'user/list-of-users',
            'module' => 'user',
            'access' => 'OPERATOR',
          ),
          array(
            'title' => _('Support'),
            'route' => 'support/form-mail',
            'module' => 'support',
            'access' => 'OPERATOR',
          ),
        ),
      ),
      /*array(
        'title' => _('Lang'),
        'route' => '#',
        'module' => '#',
        'access' => 'VISITOR',
        'pages' => array(
          array(
            'title' => _('English'),
            'route' => 'change-lang/en_US?redirect=' . $redirect,
            'module' => '#',
            'access' => 'VISITOR',
            'icon' => '<img style="width:17px; height: 17px;" src="' . $base_url . 'application/public/src/site/img/ico-usa.png"> ' . $langEn,
          ),
          array(
            'title' => _('Spanish'),
            'route' => 'change-lang/es_CO?redirect=' . $redirect,
            'module' => '#',
            'access' => 'VISITOR',
            'icon' => '<img style="width:17px; height: 17px;" src="' . $base_url . 'application/public/src/site/img/ico-spanish.png"> ' . $langCo,
          ),
          array(
            'title' => _('Frances'),
            'route' => 'change-lang/fr_FR?redirect=' . $redirect,
            'module' => '#',
            'access' => 'VISITOR',
            'icon' => '<img style="width:17px; height: 17px;" src="' . $base_url . 'application/public/src/site/img/ico-france.png"> ' . $langFr,
          ),
        ),
      ),*/  
      array(
        'title' => '%USERNAME',
        'route' => '#',
        'module' => 'user',
        'access' => 'OPERATOR',
        'pages' => array(
          /*array(
            'title' => _('Profile'),
            'route' => 'user/profile',
            'module' => 'user',
            'access' => 'VISITOR',
          ),*/
          array(
            'title' => _('Logout'),
            'route' => 'user/logout',
            'module' => 'user',
            'access' => 'OPERATOR',
          ),
        ),
      ),
    );

    $menu = array();
    return $menu;
  }


  function build_menu(){
    $html_out  = "";
    $attr_dropdown = array('class'=> 'menu-padre');

    $rol_user = 1;

    if ($this->ci->session->userdata('roles_id') !== NULL) {
      $rol_user = $this->ci->session->userdata('roles_id');
    }

    foreach ($this->options_menu() as $key => $value) {

      if ($rol_user < $this->roles[$value['access']] || ($rol_user > 1 && isset($value['id']) && $value['id'] == 'login')) {
        continue;
      }

      $icon = (isset($value['icon'])) ? $value['icon'] : '';
      $icon_drowdown = '';
      $attr = 'href="' . $value['route'] . '"';

      if (isset($value['pages'])) {
        $icon_drowdown = '<span class="glyphicon glyphicon-triangle-bottom"></span>';
      }else {
        $attr = 'href="' . base_url() . $value['route'] . '"';
      }

      if ($value['route'] == '_MODAL') {
        $attr = 'href="#" data-toggle="modal" data-target="#' . $value['id_modal'] . '"';
      }

      $html_out .= '<li><a ' . $attr . '>'. $icon . ' ' . $value['title']. ' ' . $icon_drowdown . '</a>';

      // loop through and build all the child submenus.
      $html_out .= $this->get_childs($value);
      $html_out .= '</li>';
    }

    return $html_out;
  }


  function get_childs($childs) {
    $html_out  = '';
    if (isset($childs['pages'])) {
      
      $html_out .= '<ul class="dropdown-menu">';
      
      $rol_user = 1;
      if ($this->ci->session->userdata('roles_id') !== NULL) {
        $rol_user = $this->ci->session->userdata('roles_id');
      }

      foreach ($childs['pages'] as $key => $value) {
        if ($rol_user < $this->roles[$value['access']]) {
          continue;
        }

        $icon = (isset($value['icon'])) ? $value['icon'] : '';
        $icon_drowdown = '';
        $attr = 'href="' . $value['route'] . '"';

        if (isset($value['pages'])) {
          $icon_drowdown = '<span class="glyphicon glyphicon-triangle-bottom"></span>';
        }else {
          $attr = 'href="/' . $value['route'] . '"';
        }

        if ($value['route'] == '_MODAL') {
          $attr = 'href="#" data-toggle="modal" data-target="#' . $value['id_modal'] . '"';
        }

        $html_out .= '<li><a ' . $attr . '>'. $icon . ' ' . $value['title']. ' ' . $icon_drowdown . '</a>';

        $html_out .= $this->get_childs($value);
        $html_out .= '</li>';
      }

      $html_out .= "</ul>";
    }

    return $html_out;
  }
}