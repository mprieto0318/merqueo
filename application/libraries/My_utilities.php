<?php
/**
 *
 * Created By Mprieto -  www.mprieto.co
 */
class My_utilities {

  private $ci;
  function __construct() {
    $this->ci =& get_instance();
  }

  // utilizada para redirect actual
  function get_full_url() {
    return (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  }
}