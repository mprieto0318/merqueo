<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'user/User/index';
$route['default_controller'] = 'inventario/Inventario/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['inventario'] = 'inventario/Inventario/index';
$route['inventario/ssp-index'] = 'inventario/Inventario/ssp_index';
$route['inventario/csv'] = 'inventario/Inventario/csv';


// Controller User
$route['user/login'] = 'user/User/index';
$route['user/login-process'] = 'user/User/login';
$route['user/logout'] = 'user/User/logout';
$route['user/register'] = 'user/User/register';
$route['user/recover-pass'] = 'user/User/recover_pass';
// Controller User-Admin
$route['user/list-of-users'] = 'user/Admin/index';
$route['user/ssp-list-of-users'] = 'user/Admin/ssp_list_users';
$route['user/update-user'] = 'user/Admin/update_user_ajax';
$route['user/update-security-user'] = 'user/Admin/update_security_user_ajax';
$route['user/delete-user'] = 'user/Admin/delete_user_ajax';
$route['user/create-user'] = 'user/Admin/create_user_ajax';
$route['user/change-pass/(:any)'] = 'user/Admin/change_pass_token/$1';


// Menu
$route['menu'] = 'menu/Menu/index';
$route['menu/ssp-menu'] = 'menu/Menu/ssp_menu';

// Dashboard
$route['dashboard'] = 'dashboard/Dashboard/index';

// Random
$route['random'] = 'random/Random/index';
$route['random/generate'] = 'random/Random/generate';

// Repeat
$route['repeat'] = 'repeat/Repeat/index';
$route['repeat/generate'] = 'repeat/Repeat/generate';

// Athlete
$route['athlete'] = 'athlete/Athlete/index';

// Language
$route['change-lang/(:any)'] = 'lang/Lang/index/$1';

// Support
$route['support/form-mail'] = 'support/Support/form_mail';
$route['support/send-mail/(:any)'] = 'support/Support/select_mail/$1';


// Empresas Colaboradoras
$route['empresas-colaboradoras'] = 'empresascol/Empresascol/index';
$route['empresas-colaboradoras/ssp-list'] = 'empresascol/Empresascol/ssp_list';
$route['empresas-colaboradoras/create'] = 'empresascol/Empresascol/create_ajax';
$route['empresas-colaboradoras/update'] = 'empresascol/Empresascol/update_ajax';
$route['empresas-colaboradoras/delete'] = 'empresascol/Empresascol/delete_ajax';

// Cargos
$route['cargos'] = 'cargos/Cargos/index';
$route['cargos/ssp-list'] = 'cargos/Cargos/ssp_list';
$route['cargos/create'] = 'cargos/Cargos/create_ajax';
$route['cargos/update'] = 'cargos/Cargos/update_ajax';
$route['cargos/delete'] = 'cargos/Cargos/delete_ajax';

// Nivel Academico
$route['nivel-academico'] = 'nivelacademico/Nivelacademico/index';
$route['nivel-academico/ssp-list'] = 'nivelacademico/Nivelacademico/ssp_list';
$route['nivel-academico/create'] = 'nivelacademico/Nivelacademico/create_ajax';
$route['nivel-academico/update'] = 'nivelacademico/Nivelacademico/update_ajax';
$route['nivel-academico/delete'] = 'nivelacademico/Nivelacademico/delete_ajax';

// Competencias
$route['competencias'] = 'competencias/Competencias/index';
$route['competencias/ssp-list'] = 'competencias/Competencias/ssp_list';
$route['competencias/create'] = 'competencias/Competencias/create_ajax';
$route['competencias/update'] = 'competencias/Competencias/update_ajax';
$route['competencias/delete'] = 'competencias/Competencias/delete_ajax';


// Personal
$route['personal'] = 'personal/Personal/index';
$route['personal/ssp-list'] = 'personal/Personal/ssp_list';
$route['personal/create'] = 'personal/Personal/create_ajax';
$route['personal/create-competencia'] = 'personal/Personal/create_competencia_ajax';
$route['personal/get-edit-competencia'] = 'personal/Personal/get_edit_competencia_ajax';
$route['personal/edit-competencia'] = 'personal/Personal/edit_competencia_ajax';
$route['personal/delete-competencia'] = 'personal/Personal/delete_competencia_ajax';
$route['personal/list-competencias'] = 'personal/Personal/list_competencias';
$route['personal/update'] = 'personal/Personal/update_ajax';
$route['personal/update-competencias'] = 'personal/Personal/update_competencias_ajax';
$route['personal/delete'] = 'personal/Personal/delete_ajax';
$route['personal/download-file/(:any)'] = 'personal/Personal/download_file/$1';


// Busqueda
$route['busqueda'] = 'home/Home/index';
$route['busqueda-persona'] = 'home/Home/search_byCC';
$route['reportar-competencia'] = 'home/Home/reportar_competencia';
