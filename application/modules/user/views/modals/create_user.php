<!-- Modal Editar Usuario -->
<div id="modal-create-user" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-blue">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= _('Create User'); ?></h4>
      </div>

      <div class="wrp-tabs"> 
        <ul class="nav nav-tabs">
          <li class="active">
            <a  href="#tab-create-user" data-toggle="tab"><i class="fa fa-user" aria-hidden="true"></i> <?= _('Imformation'); ?></a>
          </li>
        </ul>

          <div class="tab-content ">
            <!-- tab create user -->
            <div class="tab-pane active" id="tab-create-user">
              <?php
                $form_attr = array('id' => 'form-create-user', 'class' => 'form-horizontal');
                $label_attr = array('class' => 'col-sm-3 control-label');
              ?>
              <?= form_open('user/create-user', $form_attr); ?>
                <div class="modal-body">
                  <div class="box-body">
                    <div class="row">
                      <br>
                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Username'), 'username', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'username-modal',
                                'name' => 'username', 
                                'placeholder' => _('Enter the username'),
                                'class' => 'form-control',
                                'required' => 'required',
                                'maxlength' => 10,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Email'), 'email', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'email-modal',
                                'name' => 'email', 
                                'placeholder' => _('Enter the email'),
                                'class' => 'form-control',
                                'type' => 'email',
                                'required' => 'required',
                                'maxlength' => 50,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('First name'), 'first_name', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'first_name-modal',
                                'name' => 'first_name', 
                                'placeholder' => _('Enter the name'),
                                'class' => 'form-control',
                                'required' => 'required',
                                'maxlength' => 50,
                              );
                              ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Last name'), 'last_name', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'last_name-modal',
                                'name' => 'last_name', 
                                'placeholder' => _('Enter the surnames'),
                                'class' => 'form-control',
                                'required' => 'required',
                                'maxlength' => 50,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Phone'), 'phone', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'phone-modal',
                                'name' => 'phone', 
                                'placeholder' => _('Enter the phone'),
                                'maxlength' => 20,
                                'class' => 'form-control',
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Active'), 'active', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'active-modal',
                                'name' => 'active',
                              );
                            ?>
                            <?= form_checkbox($input, 1, NULL); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Role'), 'roles_id', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'roles_id-modal',
                                'name' => 'roles_id', 
                                'class' => 'form-control',
                                'options' => $roles,
                              );
                            ?>
                            <?= form_dropdown($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('New password'), 'new_password', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'new_password-modal',
                                'name' => 'new_password', 
                                'class' => 'form-control',
                                'type' => 'password',
                                'required' => 'required',
                                'maxlength' => 20,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Repeat new password'), 'repeat_new_password', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'repeat_new_password-modal',
                                'name' => 'repeat_new_password', 
                                'class' => 'form-control',
                                'type' => 'password',
                                'required' => 'required',
                                'maxlength' => 20,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>
                    </div>  
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="group-button text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ><?= _('Close'); ?></button>
                    <?php if($this->my_acl->access_control(FALSE, 'user', 'Admin', 'create_user_ajax')) : ?>
                      <input type="button" id="create-user" name="create-user" value="<?= _('Save'); ?>" class="btn btn-primary">
                      <div class="loading text-center" style="display: none;">
                        <br>
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <br>
                      </div>
                    <?php endif; ?>
                  </div>
                  <div id="msg-create-user" class="text-center" style="display: none;">
                    <!-- Muestra mensajes ajax -->
                  </div>
                </div>
              <?= form_close(); ?>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>