<!-- Modal -->
<div id="modal-login" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">
          <i class="fa fa-times" aria-hidden="true"></i>
        </button>
        <h4 class="modal-title"><?= _('Access'); ?></h4>
      </div>

      <div class="wrp-tabs"> 
        <ul class="nav nav-tabs">
          <li class="active">
            <a  href="#tab-login" data-toggle="tab"><i class="fa fa-user" aria-hidden="true"></i> <?= _('Login'); ?></a>
          </li>
          <li><a href="#tab-recover-pass" data-toggle="tab"><i class="fa fa-shield" aria-hidden="true"></i> <?= _('Recover password'); ?></a>
          </li>
          <li><a href="#tab-support" data-toggle="tab"><i class="fa fa-ticket" aria-hidden="true"></i> <?= _('Support'); ?></a>
          </li>
        </ul>

        <div class="tab-content ">
          <!-- tab login -->
          <div class="tab-pane active" id="tab-login">
            <?php
              $form_attr = array('id' => 'form-login', 'class' => '');
              $label_attr = array('class' => 'col-sm-3 control-label');
            ?>
            <?= form_open(base_url() . 'user/login-process', $form_attr); ?>
              <div class="modal-body">
                <div class="row">
                  <br>
                  <div class="col-md-12">
                    <div class="form-group">
                      <?= form_label(_('Email'), 'email', $label_attr); ?>
                      <div class="input-group">
                        <?php
                          $input = array(
                            'name' => 'email', 
                            'placeholder' => _('Enter the email'),
                            'class' => 'form-control',
                            'required' => 'required',
                            'type' => 'text',
                          );
                        ?>
                        <?= form_input($input); ?>
                        <span class="input-group-btn">
                          <label class="btn btn-primary">
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                          </label>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <?= form_label(_('Password'), 'pass', $label_attr); ?>
                      <div class="input-group">
                        <?php
                          $input = array(
                            'name' => 'pass', 
                            'placeholder' => _('Enter the password'),
                            'type' => 'password',
                            'class' => 'form-control',
                            'required' => 'required',
                          );
                        ?>
                        <?= form_input($input); ?>
                        <span class="input-group-btn">
                          <label class="btn btn-primary">
                            <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
                          </label>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <div class="row">
                  <div class="col-md-6">
                    <button type="button" class="btn btn-default btn-block" data-dismiss="modal"><?= _('Cancel'); ?></button>
                  </div>
                  <div class="col-md-6">
                    <?= form_submit('login', _('Enter'), 'class="btn btn-primary btn-block btn-flat"'); ?>
                  </div>
                </div>
              </div>

            <?= form_close(); ?>
          </div><!-- /tab login -->

          <!-- tab recuperar contraseña -->
          <div class="tab-pane" id="tab-recover-pass">
            <?php
              $form_attr = array('id' => 'form-recover-pass', 'class' => '');
              $label_attr = array('class' => 'col-sm-3 control-label');
            ?>
            <?= form_open(base_url() . 'user/recover-pass', $form_attr); ?>
              <div class="modal-body">
                <div class="row">
                  <br>
                  <div class="col-md-12 text-center">
                    <small>
                      <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
                      <?= _('Enter your email to send a message with the steps to follow or to recover the password'); ?>
                    </small>
                  </div>
                  <div class="col-md-offset-1 col-md-10">
                    
                    <div class="form-group">
                      <?= form_label(_('Email'), 'email', $label_attr); ?>
                      <div class="input-group">
                        <?php
                          $input = array(
                            'name' => 'email', 
                            'placeholder' => _('Enter the email'),
                            'class' => 'form-control',
                            'required' => 'required',
                            'type' => 'text',
                          );
                        ?>
                        <?= form_input($input); ?>
                        <span class="input-group-btn">
                          <label class="btn btn-primary">
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                          </label>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <div class="row">
                  <div class="col-md-6">
                    <button type="button" class="btn btn-default btn-block" data-dismiss="modal"><?= _('Cancel'); ?></button>
                  </div>
                  <div class="col-md-6">
                    <?= form_submit('login', _('Send'), 'class="btn btn-primary btn-block btn-flat"'); ?>
                  </div>
                </div>
              </div>

            <?= form_close(); ?>
          </div><!-- /tab recuperar contraseña -->

          <!-- tab soporte -->
          <div class="tab-pane" id="tab-support">
            <div class="modal-body">
              <small>
                <?= _('If you can not access or recover the password write us, we will solve it!'); ?>
              </small>
              <a href="/support/form-mail"><?= _('Contact'); ?> Web Master</a>
            </div>
            <div class="modal-footer">
              <div class="row">
                <div class="col-md-offset-6 col-md-6">
                  <button type="button" class="btn btn-default btn-block " data-dismiss="modal"><?= _('Cancel'); ?></button>
                </div>
              </div>
            </div>

          </div><!-- /tab soporte -->

        </div>
      </div>
    </div>
  </div>
</div>
 