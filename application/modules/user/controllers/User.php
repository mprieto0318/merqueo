<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Controlador para inicio y cierre de sesion
 */
class User extends MX_Controller {
 
	public function __construct() {
    parent::__construct();

    if(validate_session() && $this->uri->segment(2) != 'logout') {
      redirect('dashboard');
    }

    $this->load->model('User_model');
    $this->load->model('Admin_model');
  }
 
  // Page Login
  public function index() {
    $data = array(
      'title' => _('Log In'), 
      'show_title' => FALSE, 
      'page_render' => 'user/login',
    );

    $this->load->view('layout/template',$data);
  }

  public function login() {

    $inputs = $this->input->post();

    $valuesEmail = array(
      'email' => $inputs['email'], 
      'password' => sha1($inputs['pass']), 
      'active' => 1, 
    );

    $valuesUsername = array(
      'username' => $inputs['email'], 
      'password' => sha1($inputs['pass']), 
      'active' => 1, 
    );

    // Valida si el usuario existe
    $result = $this->User_model->login(clean_data($valuesEmail), clean_data($valuesUsername));

    if ($result) {

      $data = array(
        'id' => $result->id,
        'username' => $result->username,
        'email' => $result->email,
        'first_name' => $result->first_name,
        'last_name' => $result->last_name,
        'phone' => $result->phone,
        'last_login' => $result->last_login,
        'active' => $result->active,
        'roles_id' => $result->roles_id,
      );

      $this->session->set_userdata($data);

    	// Actualizar ultimo inicio de session
    	$this->User_model->update_last_login($result->id);

      $type = 'success';
      $msg = _('Welcome') . '<b>' . $result->username . '</b>';
      save_messages($type, $msg);

      unset($result);
      redirect('dashboard');

    }else {
      $type = 'danger';
      $msg = _('The user was not found, verify the data');
      save_messages($type, $msg);

      redirect('user/login');
    }
  }

  public function logout(){

    // - Borrar un dato de session en espefico
    // $values = array('id_u', 'name_u');
    //$this->session->unset_userdate($values);
    $this->session->sess_destroy();
    
    $type = 'info';
    $msg = _('Finished Session');
    save_messages($type, $msg);
    
    redirect('/');
  }

  public function recover_pass() {
    $inputs = $this->input->post();

    $values = array(
      'email' => $inputs['email'], 
    );

    $result = $this->Admin_model->get_users(clean_data($values));

    if ($user = $result->result()[0]) {

      $data = array(
        'token_mail' => $user->id . md5(time() . '-' . mt_rand(0, 50)),
      );
    
      $result = $this->Admin_model->update_user($user->id, $data);

      if ($result) {
        
        // Enviar mensaje
        $destiny = 'recover_pass';
        $data['username'] = $user->username;
        $data['email'] = $user->email;

        //modulo/controlador/método
        Modules::run('support/Support/select_mail', $destiny, $data);

      }else {
        $type = 'danger';
        $msg = _('The token could not be updated');
        save_messages($type, $msg);
      }
    }else {
      $type = 'danger';
      $msg = _('The email was not found, verify the data');
      save_messages($type, $msg);
    }

    redirect('/user/login');
  }
}

