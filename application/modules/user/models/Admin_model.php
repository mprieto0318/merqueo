<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

  public function __construct() {
    parent::__construct();
  }

  function getRoles($values = FALSE) {
    $q = $this->db;

    if ($values) {
      $q->where($values);
    }

    return $q->get('roles');
  }


  function ssp_list_users($values = array()) {
    // Campos DataTable
    $start = $values['start'];
    $length = $values['length'];
    $order = $values['order'];
    $search = $values['search']['value'];
    
    // Campos custom
    $dateStart = (isset($values['dateStart'])) ? $values['dateStart'] : '';
    $dateEnd = (isset($values['dateEnd'])) ? $values['dateEnd'] : '';

    $selects = 'u.id, u.username, u.email, u.phone, u.first_name, u.last_name, u.last_login, u.phone, u.active, u.roles_id, u.created_on, r.name name_role';

    $q = $this->db->select($selects);
    $q->join('roles r', 'r.id = u.roles_id');
    //$search = 'hola';
    // Filtrar por buscador
    if ($search) {
      $q->like('username', $search);
      $q->or_like('email', $search);
    }

    // Filtrar por fechas
    if (!empty($dateStart) && !empty($dateEnd)) {
      $q->where('created_on >=', $dateStart . ' 00:00:00');
      $q->where('created_on <=', $dateEnd . ' 23:59:59');
    }

    // Obtener todos los registros
    if ($length != -1) {
      $q->limit($length, $start);
    }

    // Ordenar por
    $columns = array('id', 'username', 'email', 'phone');
    $q->order_by($columns[$order[0]['column']], $order[0]['dir']);

    // Obtener SQL - DEBBUG
    //$sql = $q->get_compiled_select('users', FALSE);
    //return $sql;


    // Obtener datos
    $data = $q->get('users u');

    // Obtener datos filtrados
    $total_data_filter = $q->count_all_results();
    //$totalDatoObtenido = $resultado->num_rows();

    // ------ Obtener total de resultados
    $q->reset_query();

    $q_count = $this->db;
    //$search = 'hola';
    // Filtrar por buscador
    if ($search) {
      $q_count->like('username', $search);
      $q_count->or_like('email', $search);
    }

    // Filtrar por fechas
    if (!empty($dateStart) && !empty($dateEnd)) {
      $q_count->where('created_on >=', $dateStart . ' 00:00:00');
      $q_count->where('created_on <=', $dateEnd . ' 23:59:59');
    }

    $q_count->from('users');
    $total_data = $q_count->count_all_results();

    return array(
      'total_data' => $total_data, 
      'data' => $data,
      'total_data_filter' => $total_data_filter,
    );
  }

  function update_user($id, $values) {
    $this->db->where('id', $id);
    $this->db->update('users', $values);

    return $this->db->affected_rows();
  }

  function get_users($values = FALSE) {
    $q = $this->db;

    if ($values) {
      $q->where($values);
    }

    return $q->get('users');
  }

  function delete_users($column, $value) {
    return $this->db->delete('users', array($column => $value));
  }

  function create_user($values) {
    return $this->db->insert('users', $values);
  }

}