<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Lang extends MX_Controller {
 
  public function __construct() {
    parent::__construct();
  }
 
  public function index($lang) {

    change_language($lang);

    redirect($_GET['redirect']);
  }
}