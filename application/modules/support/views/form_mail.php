<div class="col-md-6 col-md-offset-3">
  
  <div class="panel panel-primary">
    <div class="panel-heading"><?= _('Form mail'); ?></div>
    <div class="panel-body">

      <?php
        $form_attr = array('id' => 'form-support', 'class' => '');
        $label_attr = array('class' => 'col-sm-3 control-label');
      ?>
      <?= form_open('/support/send-mail/webmaster/', $form_attr); ?>
        <div class="row">
          <br>          
          <div class="col-md-12">
            <div class="form-group">
              <?= form_label(_('Name'), 'name', $label_attr); ?>
              <div class="input-group">
                <?php
                  $input = array(
                    'name' => 'name', 
                    'placeholder' => _('Enter the name'),
                    'class' => 'form-control',
                    'required' => 'required',
                    'type' => 'text',
                  );
                ?>
                <?= form_input($input); ?>
                <span class="input-group-btn">
                  <label class="btn btn-primary">
                    <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
                  </label>
                </span>
              </div>
            </div>
            

            <div class="form-group">
              <?= form_label(_('Email'), 'email', $label_attr); ?>
              <div class="input-group">
                <?php
                  $input = array(
                    'name' => 'email', 
                    'placeholder' => _('Enter the email'),
                    'class' => 'form-control',
                    'required' => 'required',
                    'type' => 'text',
                  );
                ?>
                <?= form_input($input); ?>
                <span class="input-group-btn">
                  <label class="btn btn-primary">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                  </label>
                </span>
              </div>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <?= form_label(_('Subject'), 'subject', $label_attr); ?>
              <div class="input-group">
                <?php
                  $input = array(
                    'name' => 'subject', 
                    'placeholder' => _('Enter the subject'),
                    'class' => 'form-control',
                    'required' => 'required',
                    'type' => 'text',
                  );
                ?>
                <?= form_input($input); ?>
                <span class="input-group-btn">
                  <label class="btn btn-primary">
                    <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
                  </label>
                </span>
              </div>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <?= form_label(_('Message'), 'message', $label_attr); ?>
              <div class="input-group">
                
                
                <?php
                  $input = array(
                    'name' => 'message', 
                    'placeholder' => _('Enter the message'),
                    'type' => 'password',
                    'class' => 'form-control',
                    'required' => 'required',
                    'rows' => 4,
                  );
                ?>
                <?= form_textarea($input); ?>
                <span class="input-group-addon color-site-1"><i class="glyphicon glyphicon-comment"></i></span>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <?= form_submit('send', _('Send'), 'class="btn btn-primary btn-block btn-flat"'); ?>
          </div>
        </div>
        <?php
          $input = array(
            'type'  => 'hidden',
            'name'  => 'redirect',
            'value' => $this->my_utilities->get_full_url(),
          );
        ?>
        <?= form_input($input); ?>
      <?= form_close(); ?>

    </div>
  </div>
</div>
