<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Controlador para soporte
 */
class Support extends MX_Controller {
 
	public function __construct() {
    parent::__construct();

    //if(!validate_session() && $this->uri->segment(2) != 'form-mail') {
    //  redirect('user/login');
    //}

    $this->load->model('Support_model');
  }
 
  public function index() {
  }

  public function form_mail() {
    $data = array(
      'title' => _('Contact Form'), 
      'show_title' => TRUE, 
      'page_render' => 'support/form_mail',
      'body_class' => 'page-form-mail',
    );

    $this->load->view('layout/template',$data);
  }

  public function select_mail($destiny, $inputs = array()) {
    
    switch ($destiny) {
      case 'webmaster':
        $inputs = $this->input->post();
        $data = array(
          'template' => 'support/mail/support',
          'reply_to' => $inputs['email'],
          'to' => 'webmaster@mprieto.co',
          'name' => $inputs['name'],
          'email' => $inputs['email'],
          'subject' => $inputs['subject'],
          'message' => $inputs['message'],
          'msg_success' => _('Your message has been sent, we will respond as soon as possible'),
          'msg_failed' => _('There was a problem sending your message, please try later'),
        );

        $this->send_mail($data);

        redirect($inputs['redirect']);
        break;

      case 'recover_pass':
        $data = array(
          'template' => 'support/mail/recover_pass',
          'reply_to' => 'webmaster@mprieto.co',
          'to' => $inputs['email'],
          'subject' => _('You have requested a password change'),
          'username' => $inputs['username'],
          'token_mail' => $inputs['token_mail'],
          'msg_success' => _('an email has been sent with the instructions to recover the password. "check spam"'),
          'msg_failed' => _('The email was not found, verify the data'),
        );

        $this->send_mail($data);

        redirect('/');

        break;
      
      default:
        $type = 'danger';
        $msg = _('Could not identify is destination of the mail');
        save_messages($type, $msg); 
        break;
    }

    redirect('/');
  }

  public function send_mail($data) {
    // Configuracion
    $this->load->library("email");

    $config['protocol']    = 'smtp';
    $config['smtp_host']   = 'smtp.zoho.com';
    $config['smtp_user']   = 'site@mprieto.co';
    $config['smtp_pass']   = 'Developer0318.';
    $config['smtp_port']   = 465;
    $config['smtp_crypto'] = 'ssl';
    $config['charset']     = 'utf-8';
    $config['mailtype']    = 'html';

    $this->load->library('email');
    $this->email->initialize($config);
    $this->email->set_newline("\r\n");
    $this->email->from('site@mprieto.co', NAME_SITE);

    // Seteo de variables
    $this->email->reply_to($data['reply_to']);
    $this->email->to($data['to']);
    $this->email->subject($data['subject']);
    $this->email->message($this->load->view($data['template'],$data, TRUE));

    //$this->email->set_alt_message('This is the alternative message');
    //$this->email->cc('another@another-example.com');
    //$this->email->bcc('them@their-example.com');
    //$this->email->attach('/path/to/photo1.jpg');

    if ($this->email->send()) {
      $type = 'success';
      $msg = $data['msg_success'];
      save_messages($type, $msg);
    }else {
      $type = 'danger';
      $msg = $data['msg_failed'];
      save_messages($type, $msg); 
      //echo $this->email->print_debugger();
    }
  }
}