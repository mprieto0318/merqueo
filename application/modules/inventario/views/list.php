<div class="panel panel-default">
  <!--<div class="panel-heading">Panel Header</div>-->
  <div class="panel-body">
    <div class="col-md-12 block-site">
    	<div class="msg-ajax text-center" style="display: none;">
        
      </div>
      <div class="table-responsive">
        <table id="tbl-<?= $MODULE; ?>" class="table table-striped table-hover responsive" cellspacing="0" width="100%">
          <thead>
              <tr>
                  <!-- <th class="text-center">Id</th> -->
                  <th class="text-center"><?= _('Id'); ?></th>
                  <th class="text-center"><?= _('Nombre'); ?></th>
                  <th class="text-center"><?= _('Referencia'); ?></th>
                  <th class="text-center"><?= _('Precio'); ?></th>
                  <th class="text-center"><?= _('Costo'); ?></th>
                  <th class="text-center"><?= _('Unidades Actuales'); ?></th>
                  <th class="text-center"><?= _('Estado'); ?></th>
                  <th class="text-center"><?= _('Tipo'); ?></th>
              </tr>
          </thead>
          <tbody>
              <!-- Carga contenido via dataTables -->
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="row">
    <?php
      $form_attr = array('id' => 'form-create', 'class' => 'form-horizontal');
      $label_attr = array('class' => 'col-sm-3 control-label');
    ?>

    <?= form_open_multipart($URL_1 . '/csv', $form_attr); ?>
    <?= form_label(_('Cargar CSV'), 'csv', $label_attr); ?>

    <div class="col-md-6">
      <?php
        $input = array(
          'id' => 'csv-modal',
          'name' => 'file', 
          'class' => 'form-control',
          'type' => 'file',
        );
      ?>
      <?= form_input($input); ?>
    </div>
    <div class="col-md-3">
      <?php
        $input = array(
          'name' => 'cargar', 
          'type' => 'submit',
          'value' => 'Cargar'
        );
      ?>
      <?= form_input($input); ?>
    </div>

     <?= form_close(); ?>
  </div>

  <br>

     

</div>


<script type="text/javascript">
  // - VARIABLES TRADUCIDAS user.js
  var str_exportExcel = '<?= _('Export Excel'); ?>',
      str_exportPdf = '<?= _('Export PDF'); ?>',
      str_exportTitle = '<?= $title; ?>',
      oLanguage = {
          sProcessing:     "<?= _('Processing ...'); ?>",
          sLengthMenu:     "<?= _('Show _MENU_ records'); ?>",
          sZeroRecords:    "<?= _('No results found'); ?>",
          sEmptyTable:     "<?= _('No data available in this table'); ?>",
          sInfo:           "<?= _('Showing records from _START_ to _END_ of a total of _TOTAL_ records'); ?>",
          sInfoEmpty:      "<?= _('Showing records from 0 to 0 of a total of 0 records'); ?>",
          sInfoFiltered:   "<?= _('(filtering a total of _MAX_ records)'); ?>",
          sInfoPostFix:    "",
          sSearch:         "<?= _('Search:'); ?>",
          sUrl:            "",
          sInfoThousands:  ",",
          sLoadingRecords: "<?= _('Loading...'); ?>",
          oPaginate: {
              sFirst:    "<?= _('First'); ?>",
              sLast:     "<?= _('Latest'); ?>",
              sNext:     "<?= _('Following'); ?>",
              sPrevious: "<?= _('Previous'); ?>"
          },
          oAria: {
              sSortAscending:  "<?= _(': Activate to sort the column ascending'); ?>",
              sSortDescending: "<?= _(': Activate to order the column in descending order'); ?>"
          }
      },
      str_start = '<?= _('Start'); ?>',
      str_end = '<?= _('End'); ?>',
      str_closeText = '<?= _('Clean'); ?>',
      str_check = '<?= _('Confirm deletion'); ?>',
      str_access_denied = '<?= _('You do not have permissions for this functionality'); ?>',
      str_show_more = '<?= _('Show more'); ?>',
      str_MODULE = '<?= $MODULE ?>',
      str_URL_1 = '<?= $URL_1 ?>';
</script>