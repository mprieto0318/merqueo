<!-- Modal Editar Competencia de Persona -->
<div id="modal-edit-competencia" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-blue">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{nameCompetencia}}</h4>
      </div>

      <div class="wrp-tabs"> 
        <ul class="nav nav-tabs">
          <li>
            <a href="#tab-edit-competencia" data-toggle="tab">
              <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
              <?= _('Edit'); ?>
            </a>
          </li>
          <li class="text-danger">
            <a href="#tab-delete" data-toggle="tab"><i class="fa fa-trash-o" aria-hidden="true"></i> <?= _('Delete'); ?></a>
          </li>
        </ul>

          <div class="tab-content">
            <!-- tab edit-competencia -->
            <div class="tab-pane active" id="tab-edit-competencia">
              <?php
                $form_attr = array('id' => 'form-edit-competencia', 'class' => 'form-horizontal', 'enctype' => "multipart/form-data");
                $label_attr = array('class' => 'col-sm-3 control-label');
              ?>
              <?= form_open($URL_1 . '/edit-competencia', $form_attr); ?>
                <div class="modal-body">
                  <div class="box-body">
                    <div class="row">
                      <br>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Competition'), 'competencias_id', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'competencias_id-modal',
                                'name' => 'competencias_id', 
                                'class' => 'form-control',
                                'options' => $competencias,
                              );
                            ?>
                            <?= form_dropdown($input); ?>
                          </div>
                        </div>
                      </div>

                      <?php
                        $input = array(
                          'id'    => 'competencias_id_hidden-modal',
                          'type'  => 'hidden',
                          'name'  => 'competencias_id_hidden',
                        );
                      ?>

                      <?= form_input($input); ?>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Expedition Date'), 'expedition_date', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'expedition_date-modal',
                                'name' => 'expedition_date', 
                                'class' => 'form-control',
                                'required' => 'required',
                                'type' => 'date',                                
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Expiration Date'), 'expiration_date', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'expiration_date-modal',
                                'name' => 'expiration_date', 
                                'class' => 'form-control',
                                'type' => 'date',                                
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Validity Time'), 'validity_time', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'validity_time-modal',
                                'name' => 'validity_time', 
                                'class' => 'form-control',
                                'type' => 'number',
                                'min' => 0,
                                'max' => 50, 
                                'value' => 1,                               
                              );
                            ?>
                            <?= form_input($input); ?>
                            <small>Max: 20MB</small>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          
                          <?= form_label(_('Attached Certificate'), 'attached_certificate', $label_attr); ?>
                          <br>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'attached_certificate-modal',
                                'name' => 'file', 
                                'class' => 'form-control',
                                'type' => 'file',
                              );
                            ?>
                            <?= form_input($input); ?>
                            <br>
                            <p class="text-info"><b><?= _('Current Certificate'); ?>: </b> <a id="current_certificado" href="#" target="_blank">{{text}}</a></p>
                          </div>
                        </div>
                      </div>

                      <?php
                        $input = array(
                          'id'    => 'attached_certificate_hidden-modal',
                          'type'  => 'hidden',
                          'name'  => 'attached_certificate_hidden',
                        );
                      ?>

                      <?= form_input($input); ?>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Active'), 'active', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'active-modal',
                                'name' => 'active',
                              );
                            ?>
                            <?= form_checkbox($input, 1, NULL); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-12">
                        <div class="form-group">
                          <?= form_label(_('Comment Report'), 'comment_report', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'comment_report-modal',
                                'name' => 'comment_report', 
                                'placeholder' => _('Enter the comment report'),
                                'class' => 'form-control',
                                'maxlength' => 600,
                                'rows'        => '4',
                                'cols'        => '10',
                              );
                            ?>
                            <?= form_textarea($input); ?>
                          </div>
                        </div>
                      </div>

                      <?php
                        $input = array(
                          'id'    => 'personal_id-modal-competencia',
                          'type'  => 'hidden',
                          'name'  => 'personal_id',
                        );
                      ?>

                      <?= form_input($input); ?>

                      <?php
                        $input = array(
                          'id'    => 'id-modal',
                          'type'  => 'hidden',
                          'name'  => 'id',
                        );
                      ?>
                      <?= form_input($input); ?>
                    </div>  
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="group-button text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ><?= _('Close'); ?></button>
                    <?php if($this->my_acl->access_control(FALSE, $MODULE, $CONTROL, 'update_competencias_ajax')) : ?>
                      <input type="submit" id="btn-edit-competencias" name="btn-edit-competencias" value="<?= _('Save'); ?>" class="btn btn-primary">
                      <div class="loading text-center" style="display: none;">
                        <br>
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <br>
                      </div>
                    <?php endif; ?>
                  </div>
                  <div id="msg-edit-competencias" class="text-center" style="display: none;">
                    <!-- Muestra mensajes ajax -->
                  </div>
                </div>
              <?= form_close(); ?>
            </div>

            <!-- tab delete -->
            <div class="tab-pane" id="tab-delete">
              <?php
                $form_attr = array('id' => 'form-delete-competencia', 'class' => 'form-horizontal');
                $label_attr = array('class' => 'col-sm-3 control-label');
              ?>
              <?= form_open($URL_1 . '/delete-competencia', $form_attr); ?>
                <div class="modal-body">
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-6">
                        <br>
                        <p class="text-center text-danger">
                          <i class="fa fa-exclamation-triangle" aria-hidden="true"> </i>
                          <?= _('Once deleted, the information can not be recovered'); ?>
                        </p>
                      </div>
                    </div>  
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="group-button text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ><?= _('Close'); ?></button>
                    <?php if($this->my_acl->access_control(FALSE, $MODULE, $CONTROL, 'delete_competencia_ajax')) : ?>
                      <input type="button" id="btn-delete-competencia" name="btn-delete-competencia" value="<?= _('Delete'); ?>" class="btn btn-danger">
                      <div class="loading text-center" style="display: none;">
                        <br>
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <br>
                      </div>
                    <?php endif; ?>
                  </div>
                  <div id="msg-delete" class="text-center" style="display: none;">
                    <!-- Muestra mensajes ajax -->
                  </div>
                </div>
              <?= form_close(); ?>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>