<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventario_model extends CI_Model {

  public $tbl;

  public function __construct() {
    parent::__construct();
    $this->tbl = 'PRODUCTO';
  }


  function ssp_index($values = array()) {
    // Campos DataTable
    $start = $values['start'];
    $length = $values['length'];
    $order = $values['order'];
    $search = $values['search']['value'];

    /*$start = 0;
    $length = 10;
    $order[0]['column'] = 0;
    $order[0]['dir'] = 'ASC';
    $search = '';
    */

    // Campos custom
    $dateStart = (isset($values['dateStart'])) ? $values['dateStart'] : '';
    $dateEnd = (isset($values['dateEnd'])) ? $values['dateEnd'] : '';


    $selects = 'p.id, p.name, p.reference, p.price, p.cost, p.current_units, p.state, t.name name_tipo_producto';

    $q = $this->db->select($selects);
    $q->join('TIPO_PRODUCTO t', 't.id = p.TIPO_PRODUCTO_id');

    // Filtrar por buscador
    if ($search) {
      $q->like('p.id', $search);
      $q->or_like('p.name', $search);
      $q->or_like('p.reference', $search);
      $q->or_like('p.price', $search);
      $q->or_like('p.cost', $search);
      $q->or_like('p.current_units', $search);
      $q->or_like('t.name', $search);
    }

    // Filtrar por fechas
    if (!empty($dateStart) && !empty($dateEnd)) {
      $q->where('p.created_on >=', $dateStart . ' 00:00:00');
      $q->where('p.created_on <=', $dateEnd . ' 23:59:59');
    }

    // Obtener todos los registros
    if ($length != -1) {
      $q->limit($length, $start);
    }

    // Ordenar por
    $columns = array('id', 'name', 'reference', 'price', 'cost', 'current_units');
    $q->order_by($columns[$order[0]['column']], $order[0]['dir']);

    // Obtener SQL - DEBBUG
    //$sql = $q->get_compiled_select('users', FALSE);
    //echo $sql; exit;

    // Obtener datos
    $data = $q->get($this->tbl . ' p');

    // Obtener datos filtrados
    $total_data_filter = $q->count_all_results();
    //$totalDatoObtenido = $resultado->num_rows();

    // ------ Obtener total de resultados
    $q->reset_query();

    $q_count = $this->db;
    //$search = 'hola';
    // Filtrar por buscador
    if ($search) {
      $q->like('p.id', $search);
      $q->or_like('p.name', $search);
      $q->or_like('p.reference', $search);
      $q->or_like('p.price', $search);
      $q->or_like('p.cost', $search);
      $q->or_like('p.current_units', $search);
      $q->or_like('t.name', $search);
    }

    // Filtrar por fechas
    if (!empty($dateStart) && !empty($dateEnd)) {
      $q_count->where('p.created_on >=', $dateStart . ' 00:00:00');
      $q_count->where('p.created_on <=', $dateEnd . ' 23:59:59');
    }

    $q_count->from($this->tbl . ' p');
    $q_count->join('TIPO_PRODUCTO t', 't.id = p.TIPO_PRODUCTO_id');
    $total_data = $q_count->count_all_results();

    return array(
      'total_data' => $total_data, 
      'data' => $data,
      'total_data_filter' => $total_data_filter,
    );
  }

  function update_current_units($id, $val, $type) {
    $this->db->where('id', $id);

    switch ($type) {
      case 'ADD':
        $this->db->set('current_units', 'current_units+' . $val, FALSE);
        break;

      case 'SUBTRACT':
        $this->db->set('current_units', 'current_units-' . $val, FALSE);
        break;
      
      default:
        return false;
        break;
    }

    $this->db->update($this->tbl);

    return $this->db->affected_rows();
  }


  function update_state($id, $values) {
    $this->db->where('id', $id);
    $this->db->update($this->tbl, $values);

    return $this->db->affected_rows();
  }



}