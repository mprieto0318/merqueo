<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Inventario extends MX_Controller {
 
	public function __construct() {
    parent::__construct();

    $this->load->model('Inventario_model', 'GetModel');
  }
 
  // Page List
  public function index() {
    // call helper
    add_css(datatables_css());
    add_js(datatables_js());
    add_js(array(base_url() . 'application/public/js/custom/' . $this->router->fetch_module() . '.js'));

    $data = array(
      'MODULE' => $this->router->fetch_module(),
      'CONTROL' => $this->router->fetch_class(),
      'URL_1' => 'inventario',
      'title' => _('List of Inventario'), 
      'show_title' => TRUE, 
      'page_render' => 'inventario/list',
      'body_class' => 'page-list-' . $this->uri->segment(1),
    );

    $this->load->view('layout/template',$data);
  }

  function ssp_index() {
    if (!$this->input->is_ajax_request()) {
      exit('No direct script access allowed');
    }

    $inputs = $this->input->post();

    // Campos DataTable
    $draw = $this->input->post('draw');

    $result = $this->GetModel->ssp_index($inputs);

    $data = array();
    foreach ($result['data']->result() as $key => $value) {
      $data[$key]['id'] = $value->id;
      $data[$key]['name'] = $value->name;
      $data[$key]['reference'] = $value->reference;
      $data[$key]['price'] = $value->price;
      $data[$key]['cost'] = $value->cost;
      $data[$key]['current_units'] = $value->current_units;
      $data[$key]['state'] = $value->state;
      $data[$key]['name_tipo_producto'] = $value->name_tipo_producto;
    }

    $json_data = array(
      "draw"            => intval($draw),
      "recordsTotal"    => intval($result['total_data_filter']),
      "recordsFiltered" => intval($result['total_data']),
      "data"            => $data,
    );

    echo json_encode($json_data);
  }

  function csv() {

    $csv = $_FILES['file']['tmp_name'];

    $handle = fopen($csv,"r");
    while (($row = fgetcsv($handle, 10000, ",")) != FALSE) {
      if (count($row) == 2) {
        switch (strtoupper(trim($row[1]))) {
          case 'ACTIVAR':
            $result = $this->GetModel->update_state($row[0], array('state' => 1));
            break;
          
          case 'DESACTIVAR':
            $result = $this->GetModel->update_state($row[0], array('state' => 0));
            break;

          default:
            # code...
            break;
        }
      }else if(count($row) == 3) { 
        switch (strtoupper(trim($row[1]))) {
          case 'AGREGAR':
            $result = $this->GetModel->update_current_units($row[0], $row[2], 'ADD');
            break;
          
          case 'RESTAR':
            $result = $this->GetModel->update_current_units($row[0], $row[2], 'SUBTRACT');
            break;

          default:
            # code...
            break;
        }
      }else {
        $type = 'warning';
        $msg = _('Registro no valido: <b>' . implode(',', $row) . '</b>');
        save_messages($type, $msg);
      }
    }

    $type = 'success';
    $msg = _('Archivo Procesado');
    save_messages($type, $msg);
    redirect('/');
  }


  function download_file($file) {

    $this->load->helper('download');

    $data = file_get_contents(APPPATH_URI . '/public/src/certificados/' . $file);
    force_download($file, $data, TRUE);
  }

}