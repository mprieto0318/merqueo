<?php
  $uri_public = base_url() . 'application/public/';
?>

<!-- Navbar fixed top -->
<nav class="navbar navbar-default navbar-fixed-top navbar-inverse section-nav" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="<?= base_url(); ?>" class="navbar-brand">
        <!-- <img style="width: 50px; height: 50px;" src="<?= $uri_public ;?>src/site/img/logo.png">  -->
        <!-- <div class="wrp-logo"><img class="img-responsive" src="<?= $uri_public ;?>src/site/img/logo.png"></div> -->
        MPRIETO
      </a>
    </div>
    <div class="navbar-collapse collapse">
    
      <!-- Right nav -->
      <ul class="nav navbar-nav navbar-right">
        <?= str_replace('%USERNAME', strtoupper(substr($this->session->userdata('username'),0,10)), $this->my_menu->build_menu()); ?>
      </ul>
    
    </div><!--/.nav-collapse -->
  </div><!--/.container -->
</nav>