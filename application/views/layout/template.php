<?php
  $uri_public = base_url() . 'application/public/';
?>


<!DOCTYPE html>
<html>
  <head>

    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    
  	<title><?= NAME_SITE . ' - ' . $title; ?></title>
    <link href="<?= $uri_public ;?>src/site/img/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
    <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,400,500,600,700" rel="stylesheet">
    

    <!-- ESTILOS CONRTIB -->
  	<link rel="stylesheet" rel="stylesheet" type="text/css" href="<?= $uri_public ;?>js/contrib/bootstrap/css/bootstrap.min.css">
  	<link rel="stylesheet" href="<?= $uri_public; ?>css/contrib/font-awesome/css/font-awesome.min.css">
    
    <!-- JQuery ui -->
    <link rel="stylesheet" href="<?= $uri_public; ?>js/contrib/jquery-ui/jquery-ui.min.css">

    <!-- Smartmenus -->
    <link rel="stylesheet" href="<?= $uri_public; ?>js/contrib/smartmenus/css/jquery.smartmenus.bootstrap.css">

    <?= get_css(); ?>
    
    <!-- ESTILOS CUSTOM -->
    <link rel="stylesheet" href="<?= $uri_public; ?>/css/custom/site.css">
    <link rel="stylesheet" href="<?= $uri_public; ?>/css/custom/main.css">

    <style type="text/css">
  #wrp-busqueda-persona {

    margin-top: 10px;
  /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#b5b5b5+1,d6d6d6+48,ffffff+49,ffffff+100 */
  background: #b5b5b5; /* Old browsers */
  background: -moz-linear-gradient(top, #b5b5b5 1px, #d6d6d6 165px, #ffffff 165px, #ffffff 100%); /* FF3.6-15 */
  background: -webkit-linear-gradient(top, #b5b5b5 1px,#d6d6d6 165px,#ffffff 165px,#ffffff 100%); /* Chrome10-25,Safari5.1-6 */
  background: linear-gradient(to bottom, #b5b5b5 1px,#d6d6d6 165px,#ffffff 165px,#ffffff 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b5b5b5', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
  }
</style>

  </head>

  <body style="background-image: url('<?= $uri_public; ?>src/site/img/bg.jpg'); background-size: 100%;" class="<?= (isset($body_class)) ? $body_class : ''; ?>">

    <!-- SECCION MENU -->
    <header>
      <?= $this->load->view('layout/header'); ?>  
    </header>
    


    <!-- SECCION CONTENIDO -->
    <section class="section section-content">
      <div class="container-fluid">
        <!-- Titulo y Mensajes -->
        <div class="row block-1">
          <?php if ($show_title) : ?>
            <!-- Titulo -->
            <div class="col-md-12 wrp-title">
              <h1><?= $title; ?></h1>
            </div><!-- /Titulo -->

          <?php endif; ?>

            
          
          <!-- Mensajes -->
          <div class="col-md-12">
            <?= render_messages(); ?>
          </div><!-- /Mensajes -->
        </div><!-- /Titulo y Mensajes -->
        
        <!-- Contenido -->
        <div class="row">

          <?php

          
          ?>
          <?= $this->load->view($page_render); ?>
        </div><!-- /Contenido -->
        
      </div>
    </section>


    <!-- SECCION FOOTER -->
  	<footer class="section section-footer">
  	  <div class="container-fluid">
  	    <div class="row">
  	      <div class="col-xs-12 col-sm-6 text-left">
            <br>  	        
            <div class="row">
              <div class="col-xs-5 col-md-3">
                <a href="<?= base_url(); ?>">  
                  MPRIETO
                  <!-- <img class="img-responsive" src="<?= $uri_public ;?>src/site/img/logo.png"> -->
                </a>
              </div>

              <!-- <div class="col-xs-7 col-md-4">
                <a href="<?= base_url(); ?>">
                  <img class="img-responsive" src="<?= $uri_public ;?>src/site/img/logo-codensa.png">
                </a>
              </div> -->
            </div>
  	      </div>
  	      <div class="hidden-xs col-sm-6 text-right">
  	        <br>
            <a href="mailto:mprieto@mprieto.co"><i class="fa fa-3x fa-fw fa-envelope text-inverse"></i></a>
            <a target="_blank" href="https://www.linkedin.com/in/miguel-angel-prieto-mendez-a3a10b74/"><i class="fa fa-3x fa-fw fa-linkedin text-inverse"></i></a>
  	      </div>
  	    </div>
  	  </div>
  	</footer>

    <!-- MODALS -->
    <?php if (!validate_session()) : ?>
      <?= $this->load->view('user/modals/login'); ?>
    <?php else: ?>
      
    <?php endif; ?>


    <!-- JS CONTRIB-->
  	<script src="<?= $uri_public; ?>js/contrib/jquery/jquery-3.2.1.min.js"></script>
    <script src="<?= $uri_public; ?>js/contrib/bootstrap/js/bootstrap.min.js"></script>

    <!-- JQuery ui -->
    <script src="<?= $uri_public; ?>js/contrib/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?= $uri_public; ?>js/contrib/jquery-ui/i18n/datepicker-es.js"></script>

    <!-- Smartmenus -->
    <script src="<?= $uri_public; ?>js/contrib/smartmenus/js/jquery.smartmenus.min.js"></script>
  	<script src="<?= $uri_public; ?>js/contrib/smartmenus/js/jquery.smartmenus.bootstrap.min.js"></script>

    <?= get_js(); ?>

    <!-- JS CUSTOM -->
    <script src="<?= $uri_public; ?>js/custom/site.js"></script>

    <!-- CHAT -->
    <!--<script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: '8482f919-5a18-4127-88ff-e8508bb5a749', f: true }); done = true; } }; })();</script>-->

  </body>
</html>