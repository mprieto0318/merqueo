# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.37-MariaDB)
# Base de datos: merqueo
# Tiempo de Generación: 2018-12-23 00:33:33 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla ci_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`,`ip_address`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`)
VALUES
	('0a0c61dc55b5c2e5c32f76366747120dea416a99','127.0.0.1',1545524318,X'5F5F63695F6C6173745F726567656E65726174657C693A313534353532343331383B737563636573737C613A313A7B693A303B733A31373A224172636869766F2050726F63657361646F223B7D5F5F63695F766172737C613A313A7B733A373A2273756363657373223B733A333A226E6577223B7D'),
	('2408622b9e4b5318c81c1642ca52a610553cbca0','127.0.0.1',1545525046,X'5F5F63695F6C6173745F726567656E65726174657C693A313534353532343835383B'),
	('3074911239dbfbde28f7f04015b61a8df3846233','127.0.0.1',1545521285,X'5F5F63695F6C6173745F726567656E65726174657C693A313534353532313238353B'),
	('557f68f404d220b82c3c4e22b6cb8057982b00a9','127.0.0.1',1545524657,X'5F5F63695F6C6173745F726567656E65726174657C693A313534353532343635373B'),
	('634acda73fb988fd39b69af48435dbf2d3634d5a','127.0.0.1',1545520609,X'5F5F63695F6C6173745F726567656E65726174657C693A313534353532303630393B'),
	('66cb59eaa560ebc4c255c1cfbaa92453f979068c','127.0.0.1',1545524853,X'5F5F63695F6C6173745F726567656E65726174657C693A313534353532343635373B'),
	('c737a1435e88879e286338022e31b2275511b3cc','127.0.0.1',1545523557,X'5F5F63695F6C6173745F726567656E65726174657C693A313534353532333535373B'),
	('de3911c468392668f05b9b7bc1d24f5e8be0ff4d','127.0.0.1',1545522097,X'5F5F63695F6C6173745F726567656E65726174657C693A313534353532323039373B');

/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla PRODUCTO
# ------------------------------------------------------------

DROP TABLE IF EXISTS `PRODUCTO`;

CREATE TABLE `PRODUCTO` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL DEFAULT '',
  `reference` varchar(45) NOT NULL DEFAULT '',
  `price` int(11) DEFAULT NULL,
  `cost` int(11) DEFAULT NULL,
  `current_units` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TIPO_PRODUCTO_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_TIPO_PRODUCTO_id` (`TIPO_PRODUCTO_id`),
  CONSTRAINT `fk_TIPO_PRODUCTO_id` FOREIGN KEY (`TIPO_PRODUCTO_id`) REFERENCES `TIPO_PRODUCTO` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `PRODUCTO` WRITE;
/*!40000 ALTER TABLE `PRODUCTO` DISABLE KEYS */;

INSERT INTO `PRODUCTO` (`id`, `name`, `reference`, `price`, `cost`, `current_units`, `state`, `created_on`, `TIPO_PRODUCTO_id`)
VALUES
	(1,'leche','A001',2000,1000,44,0,'2018-12-22 17:59:20',1),
	(2,'arroz','A002',1500,800,47,1,'2018-12-22 17:59:20',1),
	(3,'pasta','A003',1600,600,80,1,'2018-12-22 17:59:20',2);

/*!40000 ALTER TABLE `PRODUCTO` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla TIPO_PRODUCTO
# ------------------------------------------------------------

DROP TABLE IF EXISTS `TIPO_PRODUCTO`;

CREATE TABLE `TIPO_PRODUCTO` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `TIPO_PRODUCTO` WRITE;
/*!40000 ALTER TABLE `TIPO_PRODUCTO` DISABLE KEYS */;

INSERT INTO `TIPO_PRODUCTO` (`id`, `name`)
VALUES
	(1,'simple'),
	(2,'compuesto');

/*!40000 ALTER TABLE `TIPO_PRODUCTO` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
